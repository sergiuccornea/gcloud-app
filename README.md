# gcloud-app

Terraform code that deploys a GCloud instance.

The code has been split into 'common' code that we should/could only deploy once and forget about it such as the networking and non-common code that in our case is the instance.

In addition to this I've included a gitlab-ci.yaml file that is used by Gitlab CI to build and deploy our code.