# Create a Google Cloud load balancer
resource "google_compute_target_http_proxy" "https" {
  name    = "${var.app_name}-https-proxy"
  url_map = google_compute_url_map.https.self_link
}

resource "google_compute_url_map" "https" {
  name            = "${var.app_name}-https-map"
  default_service = google_compute_backend_service.https.self_link
}

resource "google_compute_backend_service" "https" {
  name                  = "${var.app_name}-https-be-service"
  health_checks         = [google_compute_health_check.https_health_check.self_link]
  load_balancing_scheme = "EXTERNAL"
}

resource "google_compute_target_https_proxy" "https_proxy" {
  name             = "https-proxy"
  ssl_certificates = [google_compute_global_ssl_certificate.certificate.id]
  url_map          = google_compute_http_map.https_map.name
}

resource "google_compute_health_check" "https_health_check" {
  name = "https_health_check"

  timeout_sec        = 5
  check_interval_sec = 5

  https_health_check {
    request_path = "/"
  }
}

resource "google_compute_global_ssl_certificate" "certificate" {
  name            = "web-certificate"
  private_key_pem = filebase64sha256("my-app-key.key")
  certificate_pem = filebase64sha256("my-app.crt")
}

resource "google_compute_global_forwarding_rule" "http" {
  name                  = "http-to-https"
  load_balancing_scheme = "EXTERNAL"
  port_range            = "80"
  ip_address            = "0.0.0.0"
  target                = google_compute_target_http_proxy.https.self_link
}

resource "google_compute_http_map" "https_map" {
  name            = "https-map"
  default_service = google_compute_backend_service.https.name
}
