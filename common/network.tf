resource "google_compute_network" "vpc_network" {
  name                    = "my-app-network"
  auto_create_subnetworks = false
  mtu                     = 1460
}

resource "google_compute_subnetwork" "default" {
  name          = "my-app-subnet"
  ip_cidr_range = "10.0.1.0/24"
  region        = "eu-west1"
  network       = google_compute_network.vpc_network.id
}