# Create a single Compute Engine instance
resource "google_compute_instance" "instance" {
  name         = var.app_name
  machine_type = var.type_of_machine 
  zone         = var.region
  tags         = ["ssh"]

  boot_disk {
    initialize_params {
      image = var.image_to_use
    }
  }

  metadata_startup_script = "sudo apt-get update; sudo apt-get install -yq build-essential python3-pip rsync; pip install flask"

  network_interface {
    subnetwork = google_compute_subnetwork.default.id

    access_config {
    }
  }
}

resource "google_compute_instance_group" "https_group" {
  name = "https-group-${var.app_name}"
  named_port {
    name = "https"
    port = 443
  }
  instances = [google_compute_instance.instance.name]
}
