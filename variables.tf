variable "app_name" {
  description = "The name of the application"
}

variable "type_of_machine" {
  description = "The machine type"
  default     = "f1-micro"
}

variable "region" {
  description = "The region where to deploy the instance"
  default     = "eu-west1-a"
}

variable "image_to_use" {
  description = "Image to use for our instance"
  default     = "debian-cloud/debian-11"
}

variable "project_id" {
  description = "G project ID"
  default     = "projectA-12345"
}

